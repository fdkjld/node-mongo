# NODE/MONGO

> Простой пример web-app для ведения учета товаров.

`mongodb`
`express`
`multer`

`195.161.41.36:3000`

`TODO: remove/get image.`

Для упрощения сделал две страницы, [первая](http://195.161.41.36:3000/) страница для отображения, удаления, редактирования и создания товаров. И [вторая](http://195.161.41.36:3000/image.html) для добавления `.png` с указанием номера созданного товара.

**GET** REQUEST

~~~
.../all/
.../page/:skip/:count/
.../element/:element/
.../name/:name/
.../price/:start/:end/
.../update/:element/:name/:price/:count/
.../delete/:element
~~~

**POST** REQUEST

~~~
.../product.*/
~~~

**DB**

~~~
use admin

db.createCollection('products');
db.products.insertOne({element: 4, name: 'book2', count: 10, price: 1000});

db.createCollection('counter');
db.counter.insertOne({counter: 4});
~~~