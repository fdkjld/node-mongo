/*
	json server

	mongodb/product [element, name, price, count]
	mongodb/counter
	mongodb/files
*/

//TODO: remove/get image

async function main() {
	const mongo  = require('mongodb').MongoClient;
	const uri 	 = 'mongodb://localhost:27017/admin';
	const client = new mongo(uri);

	try {
		await client.connect();
	} 

	catch (error) {
		//err
	}




	const express = require('express')
	const 	  app = express();
	const 	 port = 3000;

	app.listen(port);




	//safe image

	let upload = require('multer')();

	app.post(/product.*/, upload.single('product'), async (request, responce, next) => {
		let element = request['path'];
		element = element.match(/product(\d+)/)[1];

		let grid = require('mongodb').GridFSBucket;
		grid = new grid(client.db('admin'));

		await grid.openUploadStream(`product${element}.png`).end(request.file.buffer);

		responce.sendStatus(200);
	});




	//web app

	app.use(express.static('/root/'));




	async function create(product) {
		await client.db('admin').collection('counter').updateOne({}, {$inc: {'counter': 1}});

		const insert = await client.db('admin').collection('counter').findOne({});

		product.element = insert.counter;

		const result = await client.db('admin').collection('products').insertOne(product);

		console.log(`new product ${result}`);
	}

	async function query(query, option) {
		const cursor  = await client.db('admin').collection('products').find(query,option).project({_id:false,element:true,name:true,count:true,price:true});
		const results = await cursor.toArray();

		return results;
	}




	//urls

	app.get('/all/', (request, responce) => {
		query({}, {}).then(function(result) {responce.send(result);});
	});

	app.get('/page/:skip/:count', (request, responce) => {
		const skip  = Number(request.params.skip);
		const count = Number(request.params.count);

		query({}, {'skip': skip, 'limit': count}).then(function(result) {responce.send(result);});
	});

	app.get('/element/:element/', (request, responce) => {
		const element = request.params.element;

		query({'element': element}, {}).then(function(result) {responce.send(result);});
	});

	app.get('/name/:name/', (request, responce) => {
		const name = request.params.name;

		query({'name': name}, {}).then(function(result) {responce.send(result);});
	});

	app.get('/price/:start/:end/', (request, responce) => {
		const start = Number(request.params.start);
		const end   = Number(request.params.end);

		query({'price': {$gt: start, $lt: end}}, {}).then(function(result) {responce.send(result);});
	});

	app.get('/access/', (request, responce) => {
		query({'count': {$gt: 0}}, {}).then(function(result) {responce.send(result);});
	});

	app.get('/add/:name/:price/:count', (request, responce) => {
		const name 	= request.params.name;
		const price = Number(request.params.price);
		const count = Number(request.params.count);

		create({'name': name, 'price': price, 'count': count});

		responce.sendStatus(200);
	});

	app.get('/update/:element/:name/:price/:count/', async (request, responce) => {
		const element = Number(request.params.element);
		const name 	  = request.params.name;
		const price   = Number(request.params.price);
		const count   = Number(request.params.count);

		const filter = {'element': element};
		const update = {$set: {'name': name, 'price': price, 'count': count}};
		const result = await client.db('admin').collection('products').updateOne(filter, update);

		responce.sendStatus(200);
	});

	app.get('/delete/:element/', async (request, responce) => {
		const element = Number(request.params.element);

		await client.db('admin').collection('products').remove({'element': element});

		responce.sendStatus(200);
	});
}

main().catch(console.error);