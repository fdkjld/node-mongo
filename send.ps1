﻿#Start $action if file change.




$file = $args[0]

$script = '.\pscp.exe'
$parm1  = '-pw'
$parm2  = '8d9236Y5c4'
$parm3  = "root@195.161.41.36:/root/$file"

& $script $parm1 $parm2 $file $parm3


$action = 'Get-Date -Format "[MM/dd/yyyy-HH:mm]";& $script $parm1 $parm2 $file $parm3;'

$global:changed = $false

function Change 
{

    param(
        [string]$file,
        [string]$action
    )

    $filepath = Split-Path $file -Parent
    $filename = Split-Path $file -Leaf
    $scriptblock = [scriptblock]::Create($action)

    $watcher = New-Object IO.FileSystemWatcher $filepath, $filename -Property @{ 
        IncludeSubdirectories = $true
        EnableRaisingEvents = $true
    }

    $onchange = Register-ObjectEvent $watcher Changed -Action { $global:changed = $true }
    $null = Register-EngineEvent PowerShell.Exiting -Action { Unregister-Event -SubscriptionId $onchange.Id }

    while ($true)
    {    
        Start-Sleep -Milliseconds 100

        if ($global:changed -eq $true)
        {  
            $global:changed = $false
            
            & $scriptblock
        }
    }
}

Change -File .\$file -Action $action